<%@ page import="net.ticket.conteste.connection.Database" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.ResultSet" %>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
    <title>Book My Ticket a Mobile App  Flat Bootstrap Responsive Website Template | Main :: W3layouts</title>
    <link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
    <link href="resources/css/style.css" rel="stylesheet" type="text/css" media="all" />
    <link href='//fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <!-- For-Mobile-Apps-and-Meta-Tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content=" Book My Ticket Widget Responsive, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //For-Mobile-Apps-and-Meta-Tags -->
</head>


<%
    if(session.getAttribute("admin")==null) {
        response.sendRedirect("adminLogin");
    }

%>
<%

    Database db = new Database();
    db.connect();
    try{



        if(request.getParameter("fileNumber")!=null && !request.getParameter("fileNumber").isEmpty()) {

            Statement st0 = db.connection.createStatement();
            String qry0 = "select max(id)+1 as mx from client";
            ResultSet rs0 = st0.executeQuery(qry0);
            rs0.next();

            Statement st1 = db.connection.createStatement();
            String qry1 = "insert into client(id,name,file_number) values("+rs0.getString("mx")+",'"+request.getParameter("name")+"','"+request.getParameter("fileNumber")+"')";
            st1.executeUpdate(qry1);
        }

        if(request.getParameter("clientId")!=null && !request.getParameter("clientId").isEmpty()) {

            Statement st0 = db.connection.createStatement();
            String qry0 = "select max(id)+1 as mx from history";
            ResultSet rs0 = st0.executeQuery(qry0);
            rs0.next();

            Statement st1 = db.connection.createStatement();
            String qry1 = "insert into history(id,datetime,description,note_to_client,client_id) " +
                    "values("+rs0.getString("mx")+",now(),'"+request.getParameter("description")
                    +"','"+request.getParameter("noteToClient")+"',"+request.getParameter("clientId")+")";
            st1.executeUpdate(qry1);
        }









%>





<body class="inner">
<!----- tabs-box ---->
<div class="tabs-box">

    <div class="clearfix"> </div>
    <div class="tab-grids">
        <div id="tab5" class="tab-grid">

            <div class="contact">
                <div style="text-align:center"><a href="adminLogin">Logout</a></div>
                <h3>Insert New Client</h3>
                <div class="cbottom">
                    <div class="cbr">
                        <form method="post" action="adminEdit">
                            <input type="text" name="name" placeholder="Client Name" required="">
                            <input type="text" name="fileNumber" placeholder="File Number" required=""><br>
                            <input type="submit" value="INSERT">
                        </form>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <h3>Update Client Information</h3>
                <div class="cbottom">
                    <div class="cbr">
                        <form method="post" action="adminEdit">
                            <select name="clientId" required>
                                <option value="">Select Client</option>
                                <%

                                    Statement st3 = db.connection.createStatement();
                                    String qry3 = "select * from client WHERE id>0";
                                    ResultSet rs3 = st3.executeQuery(qry3);

                                    while (rs3.next()) {

                                %>

                                <option value="<%=rs3.getString("id")%>"><%=rs3.getString("name")%></option>
                                <%}%>
                            </select>
                            <%--<input type="datetime" name="datetime" placeholder="Date & Time" required="">--%>
                            <input type="text" name="description" placeholder="Description" required="">
                            <input type="text" name="noteToClient" placeholder="Note to Client" required="">
                            <textarea rows="5" cols="50" placeholder="Write Your Comment Here"></textarea><br>
                            <input type="submit" value="UPDATE">
                        </form>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="big"><div style="text-align:center"><a href="adminView">View Client Table</a></div></div>
    </div>
</div>
</body>

<%
    }catch(Exception e) {
            e.printStackTrace();
    } finally {
        db.close();
    }


%>

</html>

