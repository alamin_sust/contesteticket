<%@ page import="net.ticket.conteste.web.util.AdminUtil" %>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
    <title>Book My Ticket a Mobile App  Flat Bootstrap Responsive Website Template | Main :: W3layouts</title>
    <link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
    <link href="resources/css/style.css" rel="stylesheet" type="text/css" media="all" />
    <link href='//fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <!-- For-Mobile-Apps-and-Meta-Tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content=" Book My Ticket Widget Responsive, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //For-Mobile-Apps-and-Meta-Tags -->
</head>
<body class="inner">

<%

try {

    String username = AdminUtil.USERNAME;
    String password = AdminUtil.PASSWORD;
    String infoMsg = "";

    session.setAttribute("admin",null);
    if(request.getParameter("username")!=null && !request.getParameter("username").isEmpty()) {
        if(request.getParameter("username").equals(username) && request.getParameter("password").equals(password)) {
            session.setAttribute("admin",username);
            response.sendRedirect("adminView");
        } else {
            infoMsg = "Username and Password doesn't match. Try again";
        }
    }

%>



<!----- tabs-box ---->
<div class="tabs-box">
    <div class="clearfix"> </div>
    <div class="tab-grids">

        <div id="tab4" class="tab-grid">
            <h4>LOGIN INTO ADMIN PANEL</h4>
            <h4 style="color: red"><%=infoMsg%></h4>
            <form method="post" action="adminLogin">
                <h3>USERNAME</h3>
                <input type="text" class="username" name="username"  required="">
                <h3>PASSWORD</h3>
                <input type="password" class="password" name="password" required=""><br>
                <input type="submit" value="SIGN IN"><br>
            </form>

        </div>
    </div>
</div>

<%}catch (Exception e) {

    e.printStackTrace();
} finally {

}%>
</body>
</html>

