package net.ticket.conteste.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * Created by Al-Amin on 3/8/2017.
 */
@Controller
public class HomeController {

    private static final String REDIRECT_HOME = "redirect:/home";
    private static final String REDIRECT_WELCOME = "redirect:/welcome";
    private static final String HOME = "home";
    private static final String WELCOME = "welcome";
    private static final String ADMIN_VIEW = "adminView";
    private static final String ADMIN_EDIT = "adminEdit";
    private static final String PAYMENT = "payment";
    private static final String ADMIN_LOGIN = "adminLogin";

    @GetMapping("/")
    public String goHome() {
        return REDIRECT_WELCOME;
    }

    @GetMapping("/home")
    public String home(Model model) {
        return HOME;
    }

    @GetMapping("/welcome")
    public String welcome(Model model) {
        return WELCOME;
    }

    @GetMapping("/adminView")
    public String adminView(Model model) {
        return ADMIN_VIEW;
    }

    @GetMapping("/adminEdit")
    public String adminEdit(Model model) {
        return ADMIN_EDIT;
    }

    @PostMapping("/adminEdit")
    public String adminEditPost(Model model) {
        return ADMIN_EDIT;
    }

    @GetMapping("/adminLogin")
    public String adminLogin(Model model) {
        return ADMIN_LOGIN;
    }

    @PostMapping("/adminLogin")
    public String adminLoginPost(Model model) {
        return ADMIN_LOGIN;
    }

    @GetMapping("/payment")
    public String payment(Model model) {
        return PAYMENT;
    }

}
