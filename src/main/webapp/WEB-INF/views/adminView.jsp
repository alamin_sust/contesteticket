<%@ page import="net.ticket.conteste.connection.Database" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
    <title>Book My Ticket a Mobile App  Flat Bootstrap Responsive Website Template | Main :: W3layouts</title>
    <link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
    <link href="resources/css/style.css" rel="stylesheet" type="text/css" media="all" />
    <link href='//fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <!-- For-Mobile-Apps-and-Meta-Tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content=" Book My Ticket Widget Responsive, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //For-Mobile-Apps-and-Meta-Tags -->
</head>
<body class="inner">

<%
if(session.getAttribute("admin")==null) {
    response.sendRedirect("adminLogin");
}

%>

<!----- tabs-box ---->
<div class="tabs-box">

    <div class="clearfix"> </div>
    <div style="text-align:center"><a href="adminLogin">Logout</a></div>
    <div class="tab-grids">


        <div id="tab3" class="tab-grid">

            <%
                Database db = new Database();
                db.connect();
                try{

                    Statement st1 = db.connection.createStatement();
                    String qry1 = "select * from client where id>0";
                    ResultSet rs1 = st1.executeQuery(qry1);

                    while (rs1.next()) {



            %>

            <h4><li>Client Name: <%=rs1.getString("name")%></li>
                <li>File Number: <%=rs1.getString("file_number")%></li></h4>
            <table>
                <tr>
                    <th>Date & Ttime</th>
                    <th>Descrip- tion</th>
                    <th>Note to Client</th>
                </tr>



                <tr>
                    <%

                        Statement st2 = db.connection.createStatement();
                        String qry2 = "select * from history where id>0 and client_id="+rs1.getString("id");
                        ResultSet rs2 = st2.executeQuery(qry2);
                        List<String> dateTimeList = new ArrayList<>();
                        List<String> dateList = new ArrayList<>();
                        List<String> timeList = new ArrayList<>();
                        List<String> descList = new ArrayList<>();
                        List<String> nTCList = new ArrayList<>();
                        while(rs2.next()) {
                            dateTimeList.add(rs2.getString("datetime"));
                            dateList.add(rs2.getString("datetime").substring(0,10));
                            timeList.add(rs2.getString("datetime").substring(11,19));
                            descList.add(rs2.getString("description"));
                            nTCList.add(rs2.getString("note_to_client"));
                        }

                    %>


                    <td>
                        <%
                            for (String dateTime : dateTimeList) {
                        %>
                        <a href="adminEdit"><%=dateTime%>
                        </a>
                        <%
                            }
                        %>
                    </td>
                    <td>
                        <%
                            for (String desc : descList) {
                        %>
                        <a href="adminEdit"><%=desc%>
                        </a>
                        <%
                            }
                        %>
                    </td>
                    <td>
                        <%
                            for (String nTC : nTCList) {
                        %>
                        <a href="adminEdit"><%=nTC%>
                        </a>
                        <%
                            }
                        %>
                    </td>



                </tr>



            </table>

            <%}
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                db.close();
            }
            %>

        </div>

        <div class="big"><div style="text-align:center"><a href="adminEdit">Edit Client Table</a></div></div>
    </div>
</div>
</body>
</html>

